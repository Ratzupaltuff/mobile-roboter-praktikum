#ifndef __CUSTOMDRIVEONLINE_H
#define __CUSTOMDRIVEONLINE_H

#include <stdint.h>
#include "customDrive.h"

void driveOnLine(void);
void enableDriveOnLine(void);
void disableDriveOnLine();
uint8_t isLineLost();
enum DIRECTION getDriveTurnTendency();
void setDriveTurnTendency(enum DIRECTION direction);

#endif
