#include <customLeds.h>
#include <stdint.h>
//#include "main.h"
#include "gpio.h"
#include "customDrive.h"
#include "customMath.h"

/*
 * This class controls the leds of the robot
 * you can set frequency and dutycycle of each of the three leds
 */

enum LED_STATE LED_left_state = LED_OFF;
enum LED_STATE current_LED_left_state = LED_OFF;
uint32_t LED_left_timeout = 0;
enum LED_STATE LED_right_state = LED_OFF;
enum LED_STATE current_LED_right_state = LED_OFF;
uint32_t LED_right_timeout = 0;
enum LED_STATE LED_smd_state = LED_OFF;
enum LED_STATE current_LED_smd_state = LED_OFF;
uint32_t LED_smd_timeout = 0;

float leftPercentOn = 0.5f;
float leftFrequency = 1.f;
float rightPercentOn = 0.5f;
float rightFrequency = 1.f;
float smdPercentOn = 0.5f;
float smdFrequency = 1.f;

void turnOnAllLEDs(void) {
	HAL_GPIO_WritePin(GPIOB, LED_left_Pin | LED_right_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, LEDs_back_Pin, GPIO_PIN_SET);
}

void setLeftLED(enum LED_STATE state) {
	if (state == LED_ON) {
		HAL_GPIO_WritePin(GPIOB, LED_left_Pin, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(GPIOB, LED_left_Pin, GPIO_PIN_RESET);
	}
}
void setRightLED(enum LED_STATE state) {
	if (state == LED_ON) {
		HAL_GPIO_WritePin(GPIOB, LED_right_Pin, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(GPIOB, LED_right_Pin, GPIO_PIN_RESET);
	}
}
void setSmdLED(enum LED_STATE state) {
	if (state == LED_ON) {
		HAL_GPIO_WritePin(GPIOA, LEDs_back_Pin, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(GPIOA, LEDs_back_Pin, GPIO_PIN_RESET);
	}
}

void LED_refresh() {
	if (HAL_GetTick() >= LED_left_timeout) {
		if (absoluteF(getForceLeft()) >= 0.96f) {
			LED_left_state = LED_ON;
		} else {
			LED_left_state = LED_OFF;
		}
		LED_left_timeout += 20;
	}
	if (HAL_GetTick() >= LED_right_timeout) {
		if (absoluteF(getForceRight()) >= 0.96f) {
			LED_right_state = LED_ON;
		} else {
			LED_right_state = LED_OFF;
		}
		LED_right_timeout += 20;
	}
	/*
	 switch (LED_left_state) {
	 case LED_OFF:
	 if (HAL_GetTick() >= LED_left_timeout) {
	 LED_left_timeout += leftFrequency * 1000 * leftPercentOn;
	 LED_left_state = LED_ON;
	 }
	 break;
	 case LED_ON:
	 if (HAL_GetTick() >= LED_left_timeout) {
	 LED_left_timeout += leftFrequency * 1000 * (1 - leftPercentOn);
	 LED_left_state = LED_OFF;
	 }
	 break;
	 }*/
	/*
	 switch (LED_right_state) {
	 case LED_OFF:
	 if (HAL_GetTick() >= LED_right_timeout) {
	 LED_right_timeout += rightFrequency * 1000 * rightPercentOn;
	 LED_right_state = LED_ON;
	 }
	 break;
	 case LED_ON:
	 if (HAL_GetTick() >= LED_right_timeout) {
	 LED_right_timeout += rightFrequency * 1000 * (1 - rightPercentOn);
	 LED_right_state = LED_OFF;
	 }
	 break;
	 }*/
	switch (LED_smd_state) {
	case LED_OFF:
		if (HAL_GetTick() >= LED_smd_timeout) {
			LED_smd_timeout += smdFrequency * 1000 * smdPercentOn;
			LED_smd_state = LED_ON;
		}
		break;
	case LED_ON:
		if (HAL_GetTick() >= LED_smd_timeout) {
			LED_smd_timeout += smdFrequency * 1000 * (1 - smdPercentOn);
			LED_smd_state = LED_OFF;
		}
		break;
	}
	setLeftLED(LED_left_state);
	setRightLED(LED_right_state);
	setSmdLED(LED_smd_state);
}

void initLEDs() {
	LED_left_timeout = HAL_GetTick();
	LED_right_timeout = HAL_GetTick();
	LED_smd_timeout = HAL_GetTick();
}

void setLEDOffset(float leftOff, float rightOff, float smdOff) {
	LED_left_timeout += leftOff * 1000;
	LED_right_timeout += rightOff * 1000;
	LED_smd_timeout += smdOff * 1000;
}

void setLEDTimings(float leftOn, float leftFreq, float rightOn, float rightFreq,
		float smdOn, float smdFreq) {
	if (leftOn >= 0) {
		leftPercentOn = leftOn;
	}
	if (leftFreq >= 0) {
		leftFrequency = leftFreq;
	}
	if (rightOn >= 0) {
		rightPercentOn = rightOn;
	}
	if (rightFreq >= 0) {
		rightFrequency = rightFreq;
	}
	if (smdOn >= 0) {
		smdPercentOn = smdOn;
	}
	if (smdFreq >= 0) {
		smdFrequency = smdFreq;
	}
}
