#ifndef __CUSTOMAVOIDOBSTACLE_H
#define __CUSTOMAVOIDOBSTACLE_H

#include <stdint.h>

void avoidObstacle(void);
uint8_t noAvoidanceRunning(void);
void enableAvoidObstacle(void);
void disableAvoidObstacle(void);

#endif
