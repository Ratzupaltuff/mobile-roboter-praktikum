#include <stdint.h>
#include "customDetectLine.h"
#include "main.h"
#include "customMath.h"
#include "customAdc.h"
#include "customParcour.h"

/*
 * This is a helper class to provide methods for detecting a line below the robot
 * It normalizes the adc inputs of the sensor, because the sensors have very different readouts
 */
enum LINE_COLOR {
	LINE_UNKNOWN, LINE_WHITE, LINE_BLACK
};

enum LINE_COLOR leftLineState = LINE_UNKNOWN;
float leftLine = 0;
enum LINE_COLOR middleLineState = LINE_UNKNOWN;
float middleLine = 0;
enum LINE_COLOR rightLineState = LINE_UNKNOWN;
float rightLine = 0;

const uint32_t LINE_WHITE_VAL_LEFT = 220; //1210; (with low battery)
const uint32_t LINE_WHITE_VAL_MIDDLE = 240; //990;
const uint32_t LINE_WHITE_VAL_RIGHT = 1250; //1740;

const uint32_t LINE_BLACK_VAL_LEFT = 3030; //3590;
const uint32_t LINE_BLACK_VAL_MIDDLE = 3020; //3590;
const uint32_t LINE_BLACK_VAL_RIGHT = 3290; //3590;

void normalizeLineSensorValues(uint32_t leftRawValue, uint32_t middleRawValue,
		uint32_t rightRawValue) {
	leftLine = normalizeToOne(leftRawValue, LINE_WHITE_VAL_LEFT,
			LINE_BLACK_VAL_LEFT);
	middleLine = normalizeToOne(middleRawValue, LINE_WHITE_VAL_MIDDLE,
			LINE_BLACK_VAL_MIDDLE);
	rightLine = normalizeToOne(rightRawValue, LINE_WHITE_VAL_RIGHT,
			LINE_BLACK_VAL_RIGHT);
}

void refreshLineSensors() {
	volatile uint32_t *adcVals = getADCValues();
	normalizeLineSensorValues(adcVals[5], adcVals[0], adcVals[2]);
	if (leftLine >= 0) {
		leftLineState = LINE_BLACK;
	} else {
		leftLineState = LINE_WHITE;
	}
	if (middleLine >= 0) {
		middleLineState = LINE_BLACK;
	} else {
		middleLineState = LINE_WHITE;
	}
	if (rightLine >= 0) {
		rightLineState = LINE_BLACK;
	} else {
		rightLineState = LINE_WHITE;
	}
}

uint8_t isOneLineBlack(){
	if(leftLineState == LINE_BLACK || middleLineState == LINE_BLACK || rightLineState == LINE_BLACK){
		return 1;
	}
	return 0;
}

float getBrightnessLeft(){
	return leftLine;
}

float getBrightnessMiddle(){
	return middleLine;
}

float getBrightnessRight(){
	return rightLine;
}
