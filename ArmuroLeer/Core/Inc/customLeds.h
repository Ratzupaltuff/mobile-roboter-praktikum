#ifndef __CUSTOMLEDS_H
#define __CUSTOMLEDS_H

enum LED_STATE {
	LED_ON, LED_OFF
};

void turnOnAllLEDs(void);
void setLeftLED(enum LED_STATE state);
void setRightLED(enum LED_STATE state);
void setSmdLED(enum LED_STATE state);
void LED_refresh(void);
void initLEDs();
void setLEDOffset(float leftOff, float rightOff, float smdOff);
void setLEDTimings(float leftOn, float leftFreq, float rightOn, float rightFreq,
		float smdOn, float smdFreq);

#endif
