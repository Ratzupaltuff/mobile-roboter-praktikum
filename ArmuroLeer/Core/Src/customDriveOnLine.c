#include "customDriveOnLine.h"
#include "customDetectLine.h"
#include "customDrive.h"
#include <stdint.h>
#include "customParcour.h"
#include "usart.h"
#include <stdio.h>

/**
 * This class provides a method to stay on a line
 * Because the line-sensors are very different, every one gets calibrated to -1..1
 * Then the difference of the left and the right controls the forces of the motor
 *
 * the states drive left turn and right turn has the same behavior, but are separate
 * to get a heuristic whether the controller is in a right or in a left turn
 */
enum LINE_DRIVE_STATUS_P_CONTROLLER {
	LINE_DRIVE_DISABLED_P,
	LINE_DRIVE_LEFT_TURN_P,
	LINE_DRIVE_RIGHT_TURN_P,
	LINE_DRIVE_LINE_LOST
};

enum LINE_DRIVE_STATUS_P_CONTROLLER lineDriveStatusP = LINE_DRIVE_DISABLED_P;

const float BASE_SPEED2 = 1.f;
const float BASE_SPEED_TO_MAX_DIFF = 1.f;

float driveTurnTendency = 0.5;
const float DRIVE_TURN_CHANGE_SENSITIVITY = 0.15f; //the higher, the more impact short term

char string_buf2[100];

void setDriveTurnTendency(enum DIRECTION direction){
	if (direction == LEFT){
		driveTurnTendency = 0.2;
	} else {
		driveTurnTendency = 0.8;
	}
}

void updateDriveTurnTendency(enum DIRECTION direction) {
	if (direction == LEFT) {
		driveTurnTendency = driveTurnTendency * (1-DRIVE_TURN_CHANGE_SENSITIVITY) - DRIVE_TURN_CHANGE_SENSITIVITY;
	} else {
		driveTurnTendency = driveTurnTendency * (1-DRIVE_TURN_CHANGE_SENSITIVITY) + DRIVE_TURN_CHANGE_SENSITIVITY;
	}

	//print adjustment
	//uint8_t len = sprintf((char*) string_buf2, "Turn: %lu \n",
	//		(uint32_t) (driveTurnTendency * 100.f));
	//HAL_UART_Transmit(&huart2, (uint8_t*) string_buf2, len, 1000000);

	if (driveTurnTendency > 1) {
		driveTurnTendency = 1;
	} else if (driveTurnTendency < 0) {
		driveTurnTendency = 0;
	}
}

enum DIRECTION getDriveTurnTendency() {
	if (driveTurnTendency <= 0.5) {
		return LEFT;
	} else {
		return RIGHT;
	}
}

void switchToLineDriveDisabledP(void) {
	abortDriving();
	lineDriveStatusP = LINE_DRIVE_DISABLED_P;
}

void switchToLineDriveLeftTurnP(void) {
	lineDriveStatusP = LINE_DRIVE_LEFT_TURN_P;
}

void switchToLineDriveRightTurnP(void) {
	lineDriveStatusP = LINE_DRIVE_RIGHT_TURN_P;
}

void switchToLineDriveLineLostP(void) {
	abortDriving();
	lineDriveStatusP = LINE_DRIVE_LINE_LOST;
	singalDriveOnLineFinishedLineLost();
}

float lineDriveAdjustForces(void) {
	float leftToRightDifference = (getBrightnessLeft() - getBrightnessRight())
			/ 2;
	float forceLeft = BASE_SPEED2
			- leftToRightDifference * BASE_SPEED_TO_MAX_DIFF;
	float forceRight = BASE_SPEED2
			+ leftToRightDifference * BASE_SPEED_TO_MAX_DIFF;
	setForces(forceLeft, forceRight);
	return leftToRightDifference;
}

void driveOnLinePController(void) {
	float adjustment;
	switch (lineDriveStatusP) {
	case LINE_DRIVE_DISABLED_P:
		break;
	case LINE_DRIVE_LEFT_TURN_P:
		if (!isOneLineBlack()) {
			switchToLineDriveLineLostP();
		} else {
			adjustment = lineDriveAdjustForces();
			if (adjustment < 0) {
				switchToLineDriveRightTurnP();
			}
		}
		updateDriveTurnTendency(LEFT);
		break;
	case LINE_DRIVE_RIGHT_TURN_P:
		if (!isOneLineBlack()) {
			switchToLineDriveLineLostP();
		} else {
			adjustment = lineDriveAdjustForces();
			if (adjustment >= 0) {
				switchToLineDriveLeftTurnP();
			}
		}
		updateDriveTurnTendency(RIGHT);
		break;
	case LINE_DRIVE_LINE_LOST:
		//if (isOneLineBlack()) {
		//	switchToLineDriveLeftTurnP();
		//}
		break;
	}
}

void driveOnLine(void) {
	driveOnLinePController();
}

void enableDriveOnLine(void) {
	switchToLineDriveLeftTurnP();
}

void disableDriveOnLine() {
	switchToLineDriveDisabledP();
}
