#include <stdint.h>
#include "customFindLine.h"
#include "customDrive.h"
#include "customDetectLine.h"
#include "customDriveOnLine.h"
#include "customParcour.h"
#include "customMath.h"

/**
 * This class provides a method to find the line
 * In order to do that the robot moves forward until the center of rotation
 * is above the point where it lost contact with the line
 * the the robot spins 360 degree and searches for a line
 * in order to not drive back the robot ignores a 80 degree window behind the starting point
 *
 * I decided to use this method to not loose momentum (with direction change)
 *  while searching for the line, and to be more robust with detecting fast turns
 */
enum FIND_LINE_STATUS_FOREWARD {
	FORWARD_DISABLED,
	FORWARD_LINE_FOUND,
	FORWARD_LINE_LOST,
	FORWARD_TURN_LEFT,
	FORWARD_TURN_BACK_ONE,
	FORWARD_TURN_RIGHT,
	FORWARD_TURN_BACK_TWO,
	FORWARD_NO_LINE_FOUND
};

enum FIND_LINE_STATUS_ROTATE {
	ROTATE_DISABLED,
	ROTATE_LINE_FOUND,
	ROTATE_LINE_LOST,
	ROTATE_DRIVE_FORWARD,
	ROTATE_TURN_RIGHT_ONE,
	ROTATE_BACK_BLIND_SPOT,
	ROTATE_TURN_RIGHT_TWO,
	ROTATE_NO_LINE_FOUND
};

enum FIND_LINE_STRATEGY_TYPE {
	FOREWARD_STRATEGY, ROTATE_STRATEGY
};

enum FIND_LINE_STRATEGY_TYPE find_line_strategy = ROTATE_STRATEGY;

enum FIND_LINE_STATUS_FOREWARD detectLineStatusForward = FORWARD_DISABLED;
enum FIND_LINE_STATUS_ROTATE detectLineStatusRotate = ROTATE_DISABLED;

const float FORWARD_SEARCH_ANGLE = 100.f;
const float WHEELS_TO_LINE_SENSOR_DIST = 80.f;
const float ROTATE_BLIND_ANGLE_WHEN_ROTATING = 40;
const float ROTATE_HALF_SEARCH_ANGLE = 180 - ROTATE_BLIND_ANGLE_WHEN_ROTATING;

float rotate_already_rotated = 0;

enum DIRECTION turnTendency = RIGHT;

void forwardSwitchToLineLost(void) {
	detectLineStatusForward = FORWARD_LINE_LOST;
}

void forwardSwitchToTurnLeft() {
	detectLineStatusForward = FORWARD_TURN_LEFT;
	setDriveForwardAngle(-FORWARD_SEARCH_ANGLE);
}

void forwardSwitchToTurnBackOne() {
	detectLineStatusForward = FORWARD_TURN_BACK_ONE;
	setDriveBackwardAngle(FORWARD_SEARCH_ANGLE);
}

void forwardSwitchToTurnRight() {
	detectLineStatusForward = FORWARD_TURN_RIGHT;
	setDriveForwardAngle(FORWARD_SEARCH_ANGLE);
}

void forwardSwitchToTurnBackTwo() {
	detectLineStatusForward = FORWARD_TURN_BACK_TWO;
	setDriveBackwardAngle(-FORWARD_SEARCH_ANGLE);
}

void forwardSwitchToLineFound() {
	detectLineStatusForward = FORWARD_LINE_FOUND;
	abortDriving();
	signalLineLostFinishedLineFound();
}

void forwardSwitchToNoLineFound() {
	detectLineStatusForward = FORWARD_NO_LINE_FOUND;
	abortDriving();
	signalLineLostFinishedNoLineFound();
}

void forwardSwitchToDisabled() {
	abortDriving();
	detectLineStatusForward = FORWARD_DISABLED;
}

void findLineForeward(void) {
	switch (detectLineStatusForward) {
	case FORWARD_DISABLED:
		break;
	case FORWARD_LINE_LOST:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		}
		forwardSwitchToTurnLeft();
		break;
	case FORWARD_TURN_LEFT:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			forwardSwitchToTurnBackOne();
		}
		break;
	case FORWARD_TURN_BACK_ONE:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			forwardSwitchToTurnRight();
		}
		break;
	case FORWARD_TURN_RIGHT:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			forwardSwitchToTurnBackTwo();
		}
		break;
	case FORWARD_TURN_BACK_TWO:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			forwardSwitchToNoLineFound();
		}
		break;
	case FORWARD_LINE_FOUND:
		break;
	case FORWARD_NO_LINE_FOUND:
		if (isOneLineBlack()) {
			forwardSwitchToLineFound();
		}
		break;
	}
}

void rotateSwitchToLineLost(void) {
	detectLineStatusRotate = ROTATE_LINE_LOST;
	turnTendency = -getDriveTurnTendency();
	//disableDriveOnLine();
}

void rotateSwitchToDriveForward(void) {
	detectLineStatusRotate = ROTATE_DRIVE_FORWARD;
	setDriveDistMM(WHEELS_TO_LINE_SENSOR_DIST);
}

void rotateSwitchToTurnRightOne(void) {
	detectLineStatusRotate = ROTATE_TURN_RIGHT_ONE;
	rotate_already_rotated = absoluteF(
			setDriveAngle(turnTendency * ROTATE_HALF_SEARCH_ANGLE));
}

void rotateSwitchToBackBlindSpot(void) {
	detectLineStatusRotate = ROTATE_BACK_BLIND_SPOT;
	float target = 360 - ROTATE_HALF_SEARCH_ANGLE;
	abortDriving();
	rotate_already_rotated = absoluteF(
			setDriveAngle(turnTendency * (target - rotate_already_rotated)))
			+ rotate_already_rotated;
}

void rotateSwitchToRightTwo(void) {
	detectLineStatusRotate = ROTATE_TURN_RIGHT_TWO;
	float target = 360;
	rotate_already_rotated = absoluteF(
			setDriveAngle(turnTendency * (target - rotate_already_rotated)))
			+ rotate_already_rotated;
}

void rotateSwitchToLineFound(void) {
	abortDriving();
	detectLineStatusRotate = ROTATE_LINE_FOUND;
	signalLineLostFinishedLineFound();
}

void rotateSwitchToNoLineFound(void) {
	detectLineStatusRotate = ROTATE_NO_LINE_FOUND;
	signalLineLostFinishedNoLineFound();
}
void rotateSwitchToDisabled(void) {
	detectLineStatusRotate = ROTATE_DISABLED;
	abortDriving();
}

void rotateFindLine() {
	switch (detectLineStatusRotate) {
	case ROTATE_DISABLED:
		break;
	case ROTATE_LINE_FOUND:
		if (!isOneLineBlack()) {
			rotateSwitchToLineLost();
		} else {

		}
		break;
	case ROTATE_LINE_LOST:
		if (isOneLineBlack()) {
			rotateSwitchToLineFound();
		}
		rotateSwitchToDriveForward();
		break;
	case ROTATE_DRIVE_FORWARD:
		if (isOneLineBlack()) {
			rotateSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			rotateSwitchToTurnRightOne();
		}
		break;
	case ROTATE_TURN_RIGHT_ONE:
		if (isOneLineBlack()) {
			rotateSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			rotateSwitchToBackBlindSpot();
		}
		break;
	case ROTATE_BACK_BLIND_SPOT:
		if (isDriveTaskDone()) {
			rotateSwitchToRightTwo();
		}
		break;
	case ROTATE_TURN_RIGHT_TWO:
		if (isOneLineBlack()) {
			rotateSwitchToLineFound();
		} else if (isDriveTaskDone()) {
			rotateSwitchToNoLineFound();
		}
		break;
	case ROTATE_NO_LINE_FOUND:
		break;
	}
}

void findLine(void) {
	switch (find_line_strategy) {
	case FOREWARD_STRATEGY:
		findLineForeward();
		break;
	case ROTATE_STRATEGY:
		rotateFindLine();
		break;
	}
}

uint8_t isLineFound(void) {
	if (find_line_strategy == FOREWARD_STRATEGY) {
		return (detectLineStatusForward == FORWARD_LINE_FOUND);
	}
	return (detectLineStatusRotate == ROTATE_LINE_FOUND);
}

uint8_t lineSearchUnsuccessful(void) {
	if (find_line_strategy == FOREWARD_STRATEGY) {
		return (detectLineStatusForward == FORWARD_NO_LINE_FOUND);
	}
	return (detectLineStatusRotate == ROTATE_NO_LINE_FOUND);
}

void enableFindLine() {
	forwardSwitchToLineLost();
	rotateSwitchToLineLost();
}

void disableFindLine() {
	forwardSwitchToDisabled();
	rotateSwitchToDisabled();
}

const float getWheelToSensorDist(void) {
	return WHEELS_TO_LINE_SENSOR_DIST;
}

