#include <stdint.h>

#include "customAdc.h"
#include "customAvoidObstacle.h"
#include "customDrive.h"
#include "customDetectLine.h"
#include "customFindLine.h"
#include "customDriveOnLine.h"
#include "customLeds.h"
#include "customOvercomeGap.h"
#include "tim.h"
#include "customMain.h"
#include "customParcour.h"
#include "customBlindDrive.h"
#include <stdio.h>
#include "usart.h"

uint32_t currentLoopFrequency = 0;
uint32_t measureFrequencyuntil = 0;
char string_buf3[100];


//the main loop runs with more than 11kHz
//this method was intended to measue that
void updateCurrentLoopFrequency() {
	currentLoopFrequency += 1;
	if ((int32_t) HAL_GetTick() - (int32_t) measureFrequencyuntil
			>= (int32_t) 0) {
		uint8_t len = sprintf((char*) string_buf3, "LoopFreq = %lu \n",
				currentLoopFrequency);
		HAL_UART_Transmit(&huart2, (uint8_t*) string_buf3, len, 1000000);
		currentLoopFrequency = 0;
		measureFrequencyuntil = HAL_GetTick() + 1000;
	}
}

void stopIfBatteryLow(void) {
	//3100 is ok
	//2800 makes problems
	uint32_t batteryLevel = getBatteryLevel(0);
	if (batteryLevel < 100) {
		while (1) {
			abortDriving();
			//stop if battery low
		}
	}
}

void customInitTasks(void) {
	//HAL_TIM_Base_Start(&htim1);
	//initialize Motor timers
	HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);
	//printCurrentTask();

	initLEDs();
	setLEDTimings(0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 1.f);
	//setLEDOffset(0.f, 0.33f, 0.66f);
	//uint8_t skip_blind_driving = 1;
	//uint8_t calibrationMode = 0;
	//enableFindLine();
	//enableDriveOnLine();
	startParcour();
	readADC();
	//measureFrequencyuntil = HAL_GetTick() + 1000;
}

//this is the main loop
void customPeriodicTasks(void) {
	//refresh sensors
	readADC();
	stopIfBatteryLow();
	refreshLineSensors();

	//leds
	LED_refresh();

	//drive control
	findLine();
	driveOnLine();
	overcomeGap();
	avoidObstacle();
	blindDrive();

	//execute drive tasks
	drive();

	//measure update frequency
	//updateCurrentLoopFrequency();
}
