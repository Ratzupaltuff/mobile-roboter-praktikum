#ifndef __CUSTOMMATH_H
#define __CUSTOMMATH_H

#include <stdint.h>

int32_t absolute(int32_t number);
float absoluteF(float number);
float maxF(float var1, float var2);
int32_t sign(int32_t number);
int8_t circa(float value, float target);
float degreeToRadian(float degree);
float getEpsilon(void);
float normalizeToOne(uint32_t val, uint32_t min, uint32_t max);
int32_t roundF(float number);

#endif
