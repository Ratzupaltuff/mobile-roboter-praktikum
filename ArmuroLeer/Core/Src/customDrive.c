#include <stdint.h>
#include "customDrive.h"
#include <math.h>
#include "main.h"
#include "customMath.h"
#include "customAdc.h"

/*
 * This class provides useful functions for driving the robot
 * It also tracks the movement of the robot
 */

enum DRIVE_DIRECTION {
	BACKWARD = -1, STOP = 0, FOREWARD = 1
};

enum WHEEL_COLOR {
	BLACK = 1, WHITE = 0, UNDEFINED = -1
};

enum THRESHOLD {
	UPPER_THRESHOLD = 1800, MIDDLE_THRESHOLD = 1500, LOWER_THRESHOLD = 1200
};

const uint32_t MAXTIMERVAL = 65535;

int32_t targetStepsLeft = 0;
int32_t targetStepsRight = 0;

int32_t currentMotorStepsLeft = 0;
int32_t currentMotorStepsRight = 0;

float currentMotorForceLeft = 0;
float currentMotorForceRight = 0;

float directionTrim = 0.9;

uint8_t driveTaskDone = 0;

int8_t lastValueLeft = UNDEFINED;
int32_t motorStepsLeft = 0;
int8_t lastValueRight = UNDEFINED;
int32_t motorStepsRight = 0;

//needed to translate steps moved forward to distance
const float MM_PER_WHEEL_ROTATION = 126.35f; //125.5
const uint8_t STEPS_PER_ROTATION = 24;

//neded to calculate angle when rotating
const float WHEEL_DISTANCE = 84.0f; //82.5
const float CIRCUMFERENCE_ROTATE_CIRCLE_MM = M_PI * WHEEL_DISTANCE;
const float CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM = M_PI * WHEEL_DISTANCE * 2;

//controls speed
const float BASE_SPEED = 1.f;
const float DRIVE_ADJUSTMENT_FACTOR = 0.3f; //the higher the more steering wobble

void setForceLeft(float force) { //-1..1
	float rawForce = 0;

	//normalize values to range -1..1
	if (force >= 1) {
		force = 1;
	} else if (force <= -1) {
		force = -1;
	}
	currentMotorForceLeft = force;

	if (force >= 0) {
		//forward drive
		HAL_GPIO_WritePin(GPIOA, phase2_L_Pin, GPIO_PIN_SET);
		rawForce = MAXTIMERVAL * (1 - force);
	} else {
		//backward drive
		HAL_GPIO_WritePin(GPIOA, phase2_L_Pin, GPIO_PIN_RESET);
		rawForce = MAXTIMERVAL * (-force - getEpsilon());
	}
	TIM1->CCR2 = rawForce;
}

void setForceRight(float force) {
	float rawForce = 0;

	//normalize values to range -1..1
	if (force >= 1) {
		force = 1;
	} else if (force <= -1) {
		force = -1;
	}

	currentMotorForceRight = force;

	if (force >= 0) {
		//forward drive
		HAL_GPIO_WritePin(GPIOB, phase2_R_Pin, GPIO_PIN_RESET);
		rawForce = MAXTIMERVAL * (force - getEpsilon());
	} else {
		//backward drive
		HAL_GPIO_WritePin(GPIOB, phase2_R_Pin, GPIO_PIN_SET);
		rawForce = MAXTIMERVAL * (1 + force);
	}
	TIM1->CCR3 = rawForce;
}

//set motor forces
void setForces(float leftForce, float rightForce) {
	setForceLeft(leftForce);
	setForceRight(rightForce);
}

int8_t getDriveDirectionLeft() {
	if (circa(targetStepsLeft, 0)) {
		return STOP;
	} else if (sign(targetStepsLeft) == 1) {
		return FOREWARD;
	} else {
		return BACKWARD;
	}
}

int8_t getDriveDirectionRight() {
	if (circa(targetStepsRight, 0)) {
		return STOP;
	} else if (sign(targetStepsRight) == 1) {
		return FOREWARD;
	} else {
		return BACKWARD;
	}
}

//detect white and black patches of the encoders
void measureEncoder() {
	volatile uint32_t *adcVals = getADCValues();
	if (lastValueLeft == UNDEFINED || lastValueRight == UNDEFINED) {
		if (adcVals[1] > MIDDLE_THRESHOLD) {
			lastValueLeft = BLACK;
		} else {
			lastValueLeft = WHITE;
		}
		if (adcVals[4] > MIDDLE_THRESHOLD) {
			lastValueRight = BLACK;
		} else {
			lastValueRight = WHITE;
		}
	} else {
		if (lastValueLeft == WHITE && adcVals[1] > UPPER_THRESHOLD) {
			lastValueLeft = BLACK; //black detected
			motorStepsLeft += getDriveDirectionLeft();
		} else if (lastValueLeft == BLACK && adcVals[1] <= LOWER_THRESHOLD) {
			lastValueLeft = WHITE; //white detected
			motorStepsLeft += getDriveDirectionLeft();
		}

		if (lastValueRight == WHITE && adcVals[4] > UPPER_THRESHOLD) {
			lastValueRight = BLACK; //black detected
			motorStepsRight += getDriveDirectionRight();
		} else if (lastValueRight == BLACK && adcVals[4] <= LOWER_THRESHOLD) {
			lastValueRight = WHITE; //white detected
			motorStepsRight += getDriveDirectionRight();
		}
	}
}

float mmToRotations(float distMM) {
	return distMM / MM_PER_WHEEL_ROTATION;
}

//drive abort routine
void abortDriving() {
	setForces(0, 0); //stop driving
	currentMotorStepsLeft = 0;
	motorStepsLeft = 0;
	currentMotorStepsRight = 0;
	motorStepsRight = 0;
	driveTaskDone = 1;
}

float mmToSteps(float mm) {
	return mm / MM_PER_WHEEL_ROTATION * 24;
}

void setDriveSteps(float stepsLeft, float stepsRight) {
	measureEncoder(); //initialize encoder values

	//set already done steps to 0
	motorStepsLeft = 0;
	motorStepsRight = 0;

	targetStepsLeft = stepsLeft;
	targetStepsRight = stepsRight;

	//set targets
	currentMotorStepsLeft = motorStepsLeft;
	currentMotorStepsRight = motorStepsRight;

	//reset done flag
	driveTaskDone = 0;

	setForces(BASE_SPEED * getDriveDirectionLeft(),
			BASE_SPEED * getDriveDirectionRight()); //start driving
}

float setDriveAngleSteps(float stepsLeft) {
	setDriveSteps(stepsLeft, -stepsLeft);
	return roundF(
			(stepsLeft / mmToSteps(CIRCUMFERENCE_ROTATE_CIRCLE_MM)) * 360.f); //angle rotated
}

void setDriveDistMM(float distMM) {
	int32_t targetSteps = roundF(mmToSteps(distMM));
	setDriveSteps(targetSteps, targetSteps);
}

float setDriveAngle(float angle) {
	float stepsLeft = mmToSteps((CIRCUMFERENCE_ROTATE_CIRCLE_MM / 360) * angle);
	return setDriveAngleSteps(roundF(stepsLeft));
}

//monitor step changes and adjust forces accordingly
void drive() {
	if (!driveTaskDone) {
		//refresh values
		measureEncoder();

		//if wheel rotated further
		if (currentMotorStepsLeft != motorStepsLeft
				|| currentMotorStepsRight != motorStepsRight) {

			//update variables begin --------------------
			//left rotated further
			if (currentMotorStepsLeft != motorStepsLeft) {
				currentMotorStepsLeft = motorStepsLeft;
			}
			//right rotated further
			if (currentMotorStepsRight != motorStepsRight) {
				currentMotorStepsRight = motorStepsRight;
			}
			int32_t stepsMissingLeft = absolute(
					targetStepsLeft - currentMotorStepsLeft);
			int32_t stepsMissingRight = absolute(
					targetStepsRight - currentMotorStepsRight);

			float percentDoneLeft = (float) currentMotorStepsLeft
					/ (float) targetStepsLeft;
			float percentDoneRight = (float) currentMotorStepsRight
					/ (float) targetStepsRight;
			float pseudoStepsLeft = percentDoneLeft
					* maxF(targetStepsLeft, targetStepsRight);
			float pseudoStepsRight = percentDoneRight
					* maxF(targetStepsLeft, targetStepsRight);
			float pseudoStepsDifference = absoluteF(
					pseudoStepsLeft - pseudoStepsRight);

			//update variables end    --------------------

			//states where one wheel is at target
			if (stepsMissingLeft <= 0) {
				if (stepsMissingRight <= 0) {
					//finished
					setForces(0, 0); //stop driving
					driveTaskDone = 1;
				} else {
					setForces(0, BASE_SPEED * getDriveDirectionRight());
				}
			} else if (stepsMissingRight <= 0) {
				setForces(BASE_SPEED * getDriveDirectionLeft(), 0);

				//states where both wheels need to be adjusted
			} else if ((percentDoneLeft - percentDoneRight) >= 0) {
				//right needs to spin faster
				float newForceLeft = BASE_SPEED * getDriveDirectionLeft()
						- pseudoStepsDifference * DRIVE_ADJUSTMENT_FACTOR
								* getDriveDirectionLeft();
				float newForceRight = BASE_SPEED * getDriveDirectionRight()
						+ pseudoStepsDifference * DRIVE_ADJUSTMENT_FACTOR
								* getDriveDirectionRight();
				setForces(newForceLeft, newForceRight);
			} else if ((percentDoneRight - percentDoneLeft) >= 0) {
				//left needs to spin faster
				float newForceLeft = BASE_SPEED * getDriveDirectionLeft()
						+ pseudoStepsDifference * DRIVE_ADJUSTMENT_FACTOR
								* getDriveDirectionLeft();
				float newForceRight = BASE_SPEED * getDriveDirectionRight()
						- pseudoStepsDifference * DRIVE_ADJUSTMENT_FACTOR
								* getDriveDirectionRight();
				setForces(newForceLeft, newForceRight);
			}
		}
	}
}

uint8_t isDriveTaskDone(void) {
	if (driveTaskDone) {
		return 1;
	}
	return 0;
}

float setDriveForwardAngleSteps(float stepsLeft) {
	if (stepsLeft >= 0) {
		//rotate to right -> rotate Left Wheel
		setDriveSteps(stepsLeft, 0);
		return (stepsLeft / mmToSteps(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM))
				* 360; //angle rotated
	} else {
		//rotate to left -> rotate right Wheel
		setDriveSteps(0, -stepsLeft);
		return (stepsLeft / mmToSteps(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM))
				* 360; //angle rotated
	}
}

float setDriveForwardAngle(float angle) {
	float stepsLeft = mmToSteps(
			(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM / 360) * angle);
	return setDriveForwardAngleSteps(roundF(stepsLeft));
}

float setDriveBackwardAngleSteps(float stepsLeft) {
	if (stepsLeft >= 0) {
		//rotate to right -> rotate Left Wheel
		setDriveSteps(-stepsLeft, 0);
		return (stepsLeft / mmToSteps(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM))
				* 360; //angle rotated
	} else {
		//rotate to left -> rotate right Wheel
		setDriveSteps(0, stepsLeft);
		return (-stepsLeft / mmToSteps(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM))
				* 360; //angle rotated
	}
}

float setDriveBackwardAngle(float angle) {
	float stepsLeft = mmToSteps(
			(CIRCUMFERENCE_FORWARD_ROTATE_CIRCLE_MM / 360) * angle);
	return setDriveBackwardAngleSteps(roundF(-stepsLeft));
}

void driveCircleWithDiameterAndDirection(float radius, float angle,
		enum DIRECTION direction) {
	float smallradius = radius - (WHEEL_DISTANCE / 2);
	float largeradius = radius + (WHEEL_DISTANCE / 2);
	float smallDistance = mmToSteps((2 * M_PI * smallradius) * angle / 360.f);
	float largeDistance = mmToSteps((2 * M_PI * largeradius) * angle / 360.f);
	if (direction == LEFT) {
		setDriveSteps(smallDistance, largeDistance);
	} else {
		setDriveSteps(largeDistance, smallDistance);
	}
}

//used for led lightning
float getForceLeft() {
	return currentMotorForceLeft;
}

//used for led lightning
float getForceRight() {
	return currentMotorForceRight;
}

