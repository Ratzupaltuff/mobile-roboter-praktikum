#include "customOvercomeGap.h"
#include "customParcour.h"
#include "customFindLine.h"
#include "customDrive.h"
#include "customDriveOnLine.h"

/*
 * Overcome Gap is called when a Line-Search is unsuccessful.
 * It moves forward and does a Line-Search again,
 * if the Line-search is successful it switches to Line on Line
 */
enum OVERCOME_GAP_STATE {
	OVERCOME_GAP_DISABLED,
	OVERCOME_GAP_DRIVE_FORWARD,
	OVERCOME_GAP_SEARCH_LINE,
	OVERCOME_GAP_FINISHED
};

enum OVERCOME_GAP_STATE overcomeGapState = OVERCOME_GAP_DISABLED;

void switchOvercomeGapToDisabled(void) {
	overcomeGapState = OVERCOME_GAP_DISABLED;
}

void switchOvercomeGapToDriveForward(void) {
	setDriveDistMM(100);
	overcomeGapState = OVERCOME_GAP_DRIVE_FORWARD;
}

void switchOvercomeGapToSearchLine() {
	overcomeGapState = OVERCOME_GAP_SEARCH_LINE;
	setDriveTurnTendency(RIGHT);
	enableFindLine();
}

void switchOvercomeGapToFinished() {
	overcomeGapState = OVERCOME_GAP_FINISHED;
}

void overcomeGap() {
	switch (overcomeGapState) {
	case OVERCOME_GAP_DISABLED:
		break;
	case OVERCOME_GAP_DRIVE_FORWARD:
		if (isDriveTaskDone()) {
			switchOvercomeGapToSearchLine();
		}
		break;
	case OVERCOME_GAP_SEARCH_LINE:
		if (isLineFound()) {
			switchOvercomeGapToFinished();
		} else if (lineSearchUnsuccessful()) {
			switchOvercomeGapToDriveForward();
		}
		break;
	case OVERCOME_GAP_FINISHED:
		break;
	}
}

void enableOvercomeGap(void) {
	switchOvercomeGapToDriveForward();
}

void disableOvercomeGap(void) {
	switchOvercomeGapToDisabled();
}
