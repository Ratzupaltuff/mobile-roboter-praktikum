#ifndef SRC_CUSTOMBLINDDRIVE_H_
#define SRC_CUSTOMBLINDDRIVE_H_

#include <stdint.h>

void startBlindDrive(void);
void disableBlindDrive(void);
void blindDrive(void);

#endif
