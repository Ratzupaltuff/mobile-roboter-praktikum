#ifndef __CUSTOMDRIVE_H
#define __CUSTOMDRIVE_H

#include <stdint.h>

enum DIRECTION {
	LEFT = 1,
	RIGHT = -1
};

void abortDriving(void);
void setDriveDistMM(float distMM);

float setDriveForwardAngle(float angle);
float setDriveBackwardAngle(float angle);
float setDriveAngle(float angle);
float setDriveAngleSteps(float stepsLeft);

void setDriveSteps(float stepsLeft, float stepsRight);
void drive();
uint8_t isDriveTaskDone(void);
void setForces(float leftForce, float rightForce);
void driveCircleWithDiameterAndDirection(float radius, float angle, enum DIRECTION direction);
float getForceLeft();
float getForceRight();

#endif
