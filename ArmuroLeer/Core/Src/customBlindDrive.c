#include "customBlindDrive.h"
#include "customParcour.h"
#include "customDrive.h"

/**
 * This class provides code to drive a certain pattern while not listening
 * to line sensors
 * On completion the robot continues with searching the line
 */
enum BLIND_DRIVE_STATE {
	BLIND_DRIVE_DISABLED,
	BLIND_DRIVE_FORWARD_FIRST,
	BLIND_DRIVE_ROTATE_FIRST,
	BLIND_DRIVE_FORWARD_SECOND,
	BLIND_DRIVE_ROTATE_SECOND,
	BLIND_DRIVE_FORWARD_THIRD,
	BLIND_DRIVE_DONE
};

enum BLIND_DRIVE_STATE blindDriveState = BLIND_DRIVE_DISABLED;

uint8_t skipBlindDrive = 0;

void switchBlindDriveToDisabled(void) {
	abortDriving();
	blindDriveState = BLIND_DRIVE_DISABLED;
}

void switchBlindDriveToForwardFirst(void) {
	//setDriveDistMM(470.f);
	setDriveDistMM(180.f);
	blindDriveState = BLIND_DRIVE_FORWARD_FIRST;
}

void switchBlindDriveToRotateFirst(void) {
	//setDriveAngle(150.f);
	setDriveAngle(90.f);
	blindDriveState = BLIND_DRIVE_ROTATE_FIRST;
}

void switchBlindDriveToForwardSecond(void) {
	//setDriveDistMM(355.f);
	setDriveDistMM(180.f);
	blindDriveState = BLIND_DRIVE_FORWARD_SECOND;
}

void switchBlindDriveToRotateSecond(void) {
	//setDriveAngle(-90.f);
	setDriveAngle(-60.f);
	blindDriveState = BLIND_DRIVE_ROTATE_SECOND;
}

void switchBlindDriveToForwardThird(void) {
	//setDriveDistMM(328.f);
	setDriveDistMM(434.f);
	blindDriveState = BLIND_DRIVE_FORWARD_THIRD;
}

void switchBlindDriveToDone(void) {
	abortDriving();
	blindDriveState = BLIND_DRIVE_DONE;
	signalBlindDriveFinished();
}

void blindDrive() {
	switch (blindDriveState) {
	case BLIND_DRIVE_DISABLED:
		break;
	case BLIND_DRIVE_FORWARD_FIRST:
		if (skipBlindDrive){
			switchBlindDriveToDone();
			break;
		}
		if (isDriveTaskDone()) {
			switchBlindDriveToRotateFirst();
		}
		break;
	case BLIND_DRIVE_ROTATE_FIRST:
		if (isDriveTaskDone()) {
			switchBlindDriveToForwardSecond();
		}
		break;
	case BLIND_DRIVE_FORWARD_SECOND:
		if (isDriveTaskDone()) {
			switchBlindDriveToRotateSecond();
		}
		break;
	case BLIND_DRIVE_ROTATE_SECOND:
		if (isDriveTaskDone()) {
			switchBlindDriveToForwardThird();
		}
		break;
	case BLIND_DRIVE_FORWARD_THIRD:
		if (isDriveTaskDone()) {
			switchBlindDriveToDone();
		}
		break;
	case BLIND_DRIVE_DONE:
		break;
	}
}

void startBlindDrive(void) {
	switchBlindDriveToForwardFirst();
}

void disableBlindDrive(void) {
	switchBlindDriveToDisabled();
}
