#ifndef __CUSTOMADC_H
#define __CUSTOMADC_H

#include "adc.h"

enum ADC_VALUES {
	LINE_SENSOR_LEFT = 5,
	LINE_SENSOR_MIDDLE = 0,
	LINE_SENSOR_RIGHT = 2,
	BATTERY_LEVEL = 3,
	MOTOR_ENCODER_LEFT = 1,
	MOTOR_ENCODER_RIGHT = 4
};

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc1);
volatile uint32_t* readADC(void);
volatile uint32_t* getADCValues(void);
uint8_t isBatteryVoltageCritical();
uint32_t getBatteryLevel(uint8_t printVoltage);

#endif
