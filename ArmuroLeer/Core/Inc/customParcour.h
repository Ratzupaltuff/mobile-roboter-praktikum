#ifndef SRC_CUSTOMPARCOUR_H_
#define SRC_CUSTOMPARCOUR_H_

#include <stdint.h>

void driveParcour(void);
void startParcour(void);
void signalBlindDriveFinished();
void singalDriveOnLineFinishedLineLost();
void signalLineLostFinishedLineFound();
void signalLineLostFinishedNoLineFound();
void signalOvercomeGapFinished();
void signalAvoidObstacleFinished();
void signalObstacleDetected();


#endif /* SRC_CUSTOMPARCOUR_H_ */
