#include <stdint.h>
#include "customAvoidObstacle.h"
#include "main.h"
#include "customDrive.h"
#include "customParcour.h"
#include "customDetectLine.h"
#include "customFindLine.h"


/*
 * This class listens to button presses and starts an avoidance procedure
 * This procedure includes driving backward, and turning to the right by 90 degree
 * after that the robot executes a 200 degree turn around the obstacle.
 * during the last 80 degree the robot is listening for a line
 * if a line is detected the robot turns 90 degree to the right and searches for the line again
 * on success the robot continues with driving on a line
 */
enum AVOIDANCE_STATUS {
	AVOIDANCE_DISABLED,
	BUTTON_NOT_DETECTED,
	DRIVE_BACKWARDS,
	RIGHT_TURN_ONE,
	AVOID_ROTATE_BLIND,
	AVOID_ROTATE_SENSING,
	FORWARD_SHORT_TWO,
	RIGHT_TURN_TWO,
	LINE_FOUND_AFTER_OBSTACLE,
	NO_LINE_FOUND_AFTER_OBSTACLE
};

enum AVOIDANCE_STATUS avoidObstacleStatus = AVOIDANCE_DISABLED;

const float DIAMETER_OBSTACLE_MM = 150;

uint8_t isButtonPressedLeft() {
	if (!HAL_GPIO_ReadPin(GPIOA, switch_left_Pin)) {
		return 1;
	}
	return 0;
}

uint8_t isButtonPressedMiddle() {
	if (!HAL_GPIO_ReadPin(GPIOA, switch_middle_Pin)) {
		return 1;
	}
	return 0;
}

uint8_t isButtonPressedRight() {
	if (!HAL_GPIO_ReadPin(GPIOA, switch_right_Pin)) {
		return 1;
	}
	return 0;
}

void switchToAvoidanceDisabled(void){
	avoidObstacleStatus = AVOIDANCE_DISABLED;
	abortDriving();
}

void switchToDriveBackwards(void){
	avoidObstacleStatus = DRIVE_BACKWARDS;
	setDriveDistMM(-20.f);
}

void switchToRightTurnOne(void) {
	avoidObstacleStatus = RIGHT_TURN_ONE;
	setDriveAngle(90.f);
}

void switchToAvoidRotateBlind(){
	driveCircleWithDiameterAndDirection(150.f, 140.f, LEFT);
	avoidObstacleStatus = AVOID_ROTATE_BLIND;
}

void switchToAvoidRotateSensing(){
	driveCircleWithDiameterAndDirection(150.f, 80.f, LEFT);
	avoidObstacleStatus = AVOID_ROTATE_SENSING;
}

void switchToForwardShortTwo(){
	avoidObstacleStatus = FORWARD_SHORT_TWO;
	setDriveDistMM(getWheelToSensorDist());
}
void switchToRightTurnTwo(){
	avoidObstacleStatus = RIGHT_TURN_TWO;
	setDriveAngle(120.f);
}

void switchToButtonNotDetected(){
	avoidObstacleStatus = BUTTON_NOT_DETECTED;
}

void switchToLineFoundAfterObstacle(){
	abortDriving();
	avoidObstacleStatus = LINE_FOUND_AFTER_OBSTACLE;
	signalAvoidObstacleFinished();
}

void switchToNoLineFoundAfterObstacle(){
	abortDriving();
	avoidObstacleStatus = NO_LINE_FOUND_AFTER_OBSTACLE;
	signalAvoidObstacleFinished();
}

void avoidObstacle(void) {
	switch (avoidObstacleStatus) {
	case AVOIDANCE_DISABLED:
		break;
	case BUTTON_NOT_DETECTED:
		if (isButtonPressedLeft() || isButtonPressedRight()
				|| isButtonPressedMiddle()) {
			signalObstacleDetected();
			switchToDriveBackwards();
		}
		break;
	case DRIVE_BACKWARDS:
		if (isDriveTaskDone()) {
			switchToRightTurnOne();
		}
		break;
	case RIGHT_TURN_ONE:
		if (isDriveTaskDone()) {
			switchToAvoidRotateBlind();
		}
		break;
	case AVOID_ROTATE_BLIND:
		if (isDriveTaskDone()) {
			switchToAvoidRotateSensing();
		}
		break;
	case AVOID_ROTATE_SENSING:
		if (isDriveTaskDone()) {
			switchToRightTurnTwo();
		} else if (isOneLineBlack()){
			switchToForwardShortTwo();
		}
		break;
	case FORWARD_SHORT_TWO:
		if (isDriveTaskDone()) {
			switchToRightTurnTwo();
		}
		break;
	case RIGHT_TURN_TWO:
		if (isDriveTaskDone()) {
			switchToButtonNotDetected();
		} else if (isOneLineBlack()) {
			switchToLineFoundAfterObstacle();
		}
		break;
	case LINE_FOUND_AFTER_OBSTACLE:
		switchToButtonNotDetected();
		break;
	case NO_LINE_FOUND_AFTER_OBSTACLE:
		switchToButtonNotDetected();
		break;
	}
}

uint8_t noAvoidanceRunning(void){
	if(avoidObstacleStatus == BUTTON_NOT_DETECTED){
		return 1;
	}
	return 0;
}

void enableAvoidObstacle(void){
	switchToButtonNotDetected();
}

void disableAvoidObstacle(void){
	switchToAvoidanceDisabled();
}
