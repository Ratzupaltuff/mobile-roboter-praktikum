#ifndef __CUSTOMDETECTLINE_H
#define __CUSTOMDETECTLINE_H

#include <stdint.h>

void refreshLineSensors(void);
uint8_t isOneLineBlack();
float getBrightnessLeft();
float getBrightnessMiddle();
float getBrightnessRight();

#endif
