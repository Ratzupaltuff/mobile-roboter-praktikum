#include <stdint.h>
#include "main.h"
#include "adc.h"
#include "usart.h"
#include "customAdc.h"
#include <stdio.h>

/*
 * This class is like a library to everything concerning reading the adc
 */

volatile uint32_t adc[6];
uint32_t buffer[6];
volatile uint8_t conversion_done_flag = 1;
char string_buf[100];
uint8_t batteryCritical = 0;
float batteryCriticalLevel = 200;

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc1) {
	for (uint8_t i = 0; i < 6; i++) {
		adc[i] = buffer[i];
	}
	conversion_done_flag = 1;
}

/*
 * this is the only method that reads the adc,
 * it gets executed every time the main loop starts
 */
volatile uint32_t* readADC() {
	while (!conversion_done_flag) {
	}
	if (conversion_done_flag) {
		conversion_done_flag = 0;
		HAL_ADC_Start_DMA(&hadc1, buffer, 6);
	}
	uint32_t batLevel = adc[BATTERY_LEVEL];
	if (batLevel < batteryCriticalLevel) {
		batteryCritical = 1;
	}
	return adc;
}

volatile uint32_t* getADCValues() {
	return adc;
}

void printLeftMotorEnc(void) {
	uint8_t len = sprintf((char*) string_buf, "Left:%lu \n",
			adc[MOTOR_ENCODER_LEFT]);
	HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);

}

void printRightMotorEnc(void) {
	uint8_t len = sprintf((char*) string_buf, "Right:%lu \n",
			adc[MOTOR_ENCODER_RIGHT]);
	HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);
}

void printMessage(char text[]) {
	uint8_t len = sprintf((char*) string_buf, text);
	HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);
}

void printMotorEncsCSV(void) {
	printMessage("begin");
	for (uint8_t i = 0; i < 6000; i++) {
		readADC();
		uint8_t len = sprintf((char*) string_buf, "%lu,%lu\n",
				adc[MOTOR_ENCODER_LEFT], adc[MOTOR_ENCODER_RIGHT]);
		HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);
	}
	printMessage("end");
}

/*
 void printCurrentPos(void) {
 if (conversion_done_flag) {
 conversion_done_flag = 0;
 HAL_ADC_Start_DMA(&hadc1, buffer, 6);
 uint8_t len = sprintf((char*) string_buf, "Left: %lu, Right: %lu \n",
 motorStepsLeft, motorStepsRight);
 HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);
 }
 }*/

uint32_t getBatteryLevel(uint8_t printVoltage) {
	//readADC();
	if (printVoltage) {
		uint8_t len = sprintf((char*) string_buf, "BatteryVoltage:%lu \n",
				adc[BATTERY_LEVEL]);
		HAL_UART_Transmit(&huart2, (uint8_t*) string_buf, len, 1000000);
	}
	return adc[BATTERY_LEVEL];
}

/*
 float getBatteryVoltage(uint8_t printVoltage) {
 return (float) getBatteryLevel(printVoltage);
 }*/

uint8_t isBatteryVoltageCritical() {
	return batteryCritical;
}
