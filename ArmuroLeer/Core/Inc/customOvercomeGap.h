#ifndef __CUSTOMOVERCOMEGAP_H
#define __CUSTOMOVERCOMEGAP_H

#include <stdint.h>

void enableOvercomeGap();
void disableOvercomeGap();
void overcomeGap(void);

#endif
