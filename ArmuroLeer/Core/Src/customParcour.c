#include "customParcour.h"
#include <stdint.h>
#include "customBlindDrive.h"
#include "customFindLine.h"
#include "customOvercomeGap.h"
#include "customDriveOnLine.h"
#include "customAvoidObstacle.h"
#include "tim.h"


/**
 * This class provides the knowledge in which order the tasks are executed
 *
 * The tasks are changed with methods that work as triggers to change state
 */
enum DRIVING_STATE {
	DRIVING_STATE_BLIND,
	DRIVING_STATE_ON_LINE,
	DRIVING_STATE_LINE_SEARCH,
	DRIVING_STATE_OVERCOME_GAP
};

enum DRIVING_STATE parcourDrivingState = DRIVING_STATE_BLIND;

void busyWait(uint32_t millis){
	uint32_t targetTicks = HAL_GetTick() + millis;
	while (HAL_GetTick() <= targetTicks){

	}
}

//wait 2s at the beginning of the parcour
void switchToDrivingStateBlind() {
	busyWait(2000);
	enableAvoidObstacle();
	startBlindDrive();
}

void switchToDrivingOnLine() {
	enableDriveOnLine();
}

void switchToLineSearch() {
	enableFindLine();
}

void switchToOvercomeGap() {
	enableOvercomeGap();
}

//currently not used
void driveParcour() {
	switch (parcourDrivingState) {
	case DRIVING_STATE_BLIND:
		break;
	case DRIVING_STATE_ON_LINE:
		break;
	case DRIVING_STATE_LINE_SEARCH:
		break;
	case DRIVING_STATE_OVERCOME_GAP:
		break;
	}
}

void signalBlindDriveFinished() {
	switchToLineSearch();
}

void singalDriveOnLineFinishedLineLost() {
	switchToLineSearch();
}

void signalLineLostFinishedLineFound() {
	//let overcomeGap handle the task if line is found
	if(parcourDrivingState != DRIVING_STATE_OVERCOME_GAP){
		switchToDrivingOnLine();
	}
}

void signalLineLostFinishedNoLineFound() {
	switchToOvercomeGap();
}

void signalOvercomeGapFinished() {
	switchToLineSearch();
}

void signalAvoidObstacleFinished() {
	switchToLineSearch();
}

void signalObstacleDetected(){
	disableBlindDrive();
	disableDriveOnLine();
	disableFindLine();
}

void startParcour(void){
	switchToDrivingStateBlind();
}
