ÜB3

1.1 P=U*I, U=R*I, P= 2 * Pi * M * n  (M Drehmoment, n Drehzahl)

Strom und Drehmoment sind direkt proportional zueinander, fließt mehr Strom übt der Motor mehr Drehmoment aus. Drehmoment und Drehzahl sind antiproportional zueinander, man kann also entweder bei einer langsamen Drehzahl mehr Drehmoment ausüben oder bei wenig Drehmoment den Motor schnell drehen lassen. Durch erhöhen der Spannung lässt sich der Quotient aus Drehmoment und Drehzahl erhöhen. Also kann man durch erhöhen der Spannung bei gleichbleibender Drehzahl den Drehmoment erhöhen. In der Praxis stellt sich eine gewisse Drehzahl und damit auch ein gewisser Drehmoment ein. Dieser hängt von den Motoreigenschaften und der kokreten Last ab.

1.2

Die H-Brücke wird benötigt um den Motor in beide Richtungen zu be- und entschleunigen. Dazu werden vier Transistoren derart verschaltet, dass sie, wenn sie entsprechend angesteuert werden, die Spannung in beide Richtungen um den Motor anlegen können. Man kann nun also steuern ob auf beiden Seiten Versorgungsspannung, Masse oder eben die Kombinationen aus beidem anliegen. Immer wenn Versorgungsspannung und Masse anliegen, egal in welcher Richtung, wird der Motor anfangen sich zu drehen.

1.3

T1 ist ein PNP Transistor und schaltet durch wenn keine Spannung angelegt wird und sperrt wenn Spannung angelegt wird. T3 ist ein NPN Transistor und arbeitet genau entgegengesetzt, sperrt also wenn keine Spannung angelegt wird.

1.4

Da der Motor simplifiziert eine Spule ist, erzeugt er eine Spannung wenn man ihn abschaltet. Diese Spannung kann so groß werden, dass diese die zulässige Spannung für den Transistor überschreitet. Deshalb verwendet man eine sog. Schutzdiode/Freilaufdiode um die Spannung auf die Versorgungsspannung plus die Schaltspannung der Diode zu begrenzen. Bei uns beispielsweise 5V + 0.7V also 5.7V.

1.5

Wir haben festgestellt, dass T1 und T2 schalten in der Ruhestellung durch. Es liegt also auf beiden Seiten des Motors Versorgungsspannung an. Dadurch dreht sich der Motor nicht, da kein Potenzialunterschied zwischen den Kontakten vorliegt. Wir wollen nun an einen Kontakt Masse anlegen. Wollen wir also in die "normale" Richtung Spannung anlegen müssen wir zunächst Spannung an T2 anlegen, dann ist die negative Seite des Motors hochohmig. Anschließend müssen wir noch T4 durchschalten, damit die negative Seite auf Masse gezogen wird. Nun sind die Steuerpins von T2 und T4 verbunden und liegen am Buffergate an Pin 6/2Y an.

Wenn alle Transistoren sperren, kann die vom drehenden Motor induzierte Spannung über die Dioden abfließen und der Motor wird ausgebremst. Um das zu erreichen müsste man T1 und T2 schalten. Dies geht aber nicht ohne auch T3 und T4 zu schalten.

Die andere Option ist anstatt den Motor auszukoppeln einfach ihn an beiden Enden an Masse oder an Versorgungsspannung anzuschließen und somit die induzierte Spannung abzuführen. Dies können wir sehr einfach realisieren indem wir entweder beide Pins 1Y und 2Y auf Low ziehen oder beide auf High ziehen. Im ersteren Fall liegt um den Motor Versorgungsspannung an und im letzteren Fall Masse.

1.6

Ein Blick in das Datenblatt verrät, dass wir den Pin 2Y Schalten können wenn wir Pin 5/2A schalten. Dieser ist auf dem Microkontroller an Pin 4/D2 angeschlossen. Das bedeutet wir können den Motor in die eine Richtung drehen lassen wenn wir D2 auf High schalten. Analog können wir den Motor in die entgegengesetzte Richtung drehen wenn wir D3 auf High schalten.

1.7

Wenn wir nun um in die eine Richtung zu drehen nicht durchgehend den zugehörigen Pin auf High ziehen, sondern ihn ganz schnell zwischen High und Low wechseln lassen, dann wechselt der Motor zwischen Drehen und Bremsen. Das bedeutet der Motor dreht sich langsamer, als wenn wir Vollgas geben würden, aber immernoch ein wenig, da wir zumindest eine gewisse Zeit lang den Pin auf High schalten. Durch das Verhältnis zwischen High und Low Zeiten auf dem Pin lässt sich nun die Geschwindigkeit steuern. Diese Technik nennt man PWM oder auch Pulsweitenmodulation. Das Buffergate beeinflusst die PWM-Signale nur durch eine zusätzliche Latenz, die aber vernachlässigbar klein ist für unsere Anwendung.

2.3

PB1 wurde die Funktion zugeordnet ein PWM-Signal zu erzeugen. Dies ergibt auch Sinn, wenn wir nämlich in den Schaltplan schauen, ist PB1 mit dem Pin D6 verbunden, der den rechten Motor in eine Richtung drehen kann.

3.1

Die HAL_ADC_ConvCptlCallback Funktion wird aufgerufen wenn der Befehl HAL_ADC_Start_DMA(...) fertig ist und die ADC Werte vorliegen (die Konvertierung komplett ist).

3.2

Dieses Stichwort zwingt den Compiler jedes mal den Wert auszulesen und nicht anzunehmen, dass der Wert sich nach dem letzten mal schreiben nicht geändert hat. Das macht man, da die Register in denen adc[] liegt von einer anderen Komponente auf dem Chip geschrieben werden. Konkret werden sie parallel von der HAL_ADC_ConvCptlCallback() und der main() Methode geschrieben.

3.3

Wir lesen entweder in der Datei adc.c oder in der IOC Datei die Zuordnung Rank zu ADC-Channel ab. Mit dem Schaltplan können wir die Komponenten den Pins zuordnen. Wenn wir dann in der IOC-Datei nachschauen auf was der Pin konfiguriert ist sehen wir zB. "ADC_IN..." und können dort den ADC-Channel ablesen

Rank 1 - Channel 5 - PA0 - lineSensor_middle - Liniensensor mitte

Rank 2 - Channel 6 - PA1 - encoder_left - Motorencoder rechts

Rank 3 - Channel 8 - PA3 - lineSensor_right - Liniensensor rechts

Rank 4 - Channel 9 - PA4 - vbat_div - Versorgungsspannug

Rank 5 - Channel 10 - PA5 - encoder_right - Motorencoder rechts

Rank 6 - Channel 12 - PA7 - lineSensor_left - Liniensensor links
