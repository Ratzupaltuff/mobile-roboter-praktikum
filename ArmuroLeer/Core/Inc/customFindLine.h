#ifndef __CUSTOMFINDLINE_H
#define __CUSTOMFINDLINE_H

#include <stdint.h>

void findLine(void);
uint8_t isLineFound(void);
void enableFindLine(void);
void disableFindLine(void);
uint8_t lineSearchUnsuccessful(void);
const float getWheelToSensorDist(void);

#endif
