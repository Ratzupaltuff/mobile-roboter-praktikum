#include <math.h>
#include <stdint.h>
#include "customMath.h"


/*
 * I had problems including the math library into this project
 * so I reimplemented the functions I needed
 */
const float EPSILON = 0.0001;

//round a float to the next integer
//this makes steps to cm calculations more precise than just throwing away digits after the dot
int32_t roundF(float number) {
	int32_t intNumber = (int32_t) number;
	if (number >= 0) {
		if ((number - ((float) intNumber))
				<= (((float) intNumber + 1) - number)) {
			return intNumber;
		} else {
			return intNumber + 1;
		}
	} else {
		if ((((float) intNumber) - number)
				<= ((number - (float) (intNumber - 1)))) {
			return intNumber;
		} else {
			return intNumber - 1;
		}
	}
}

int32_t absolute(int32_t number) {
	if (number >= 0) {
		return number;
	} else {
		return -number;
	}
}

float absoluteF(float number) {
	if (number >= 0) {
		return number;
	} else {
		return -number;
	}
}

float maxF(float var1, float var2) {
	if (var1 >= var2) {
		return var1;
	} else {
		return var2;
	}
}

int32_t sign(int32_t number) {
	if (number >= 0) {
		return 1;
	} else {
		return -1;
	}
}

int8_t circa(float value, float target) {
	float difference = target - value;
	if (difference < EPSILON && difference > -EPSILON) {
		return 1; //true
	} else {
		return 0; //false
	}
}

float degreeToRadian(float degree) {
	return degree * 2 * M_PI / 360;
}

float getEpsilon(void) {
	return EPSILON;
}

float normalizeToOne(uint32_t val, uint32_t min, uint32_t max) {
	float halfMaxDiff = ((float) max - (float) min) / 2.f;
	float out = (((float) val - (float) min) - halfMaxDiff) / halfMaxDiff;
	if (out > 1) {
		return 1;
	} else if (out < -1) {
		return -1;
	}
	return out;
}
